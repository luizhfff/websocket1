#!/bin/sh
# THIS SCRIPT IS INTENDED TO BE RUN FROM THE websocket1 PROJECT DIRECTORY
docker run --rm -it --network my-net --name fancy-chat \
    -v "$PWD"/fancy:/mnt/fancy \
    -p 80:3000 \
    -w /mnt/fancy     bkoehler/feathersjs sh
