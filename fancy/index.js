// Setup basic express server
var express = require('express');
var app = express();
var path = require('path');
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var port = process.env.PORT || 3000;

server.listen(port, () => {
  console.log('Server listening at port %d', port);
});

// Routing
app.use(express.static(path.join(__dirname, 'public')));

// Chatroom

var numUsers = 0;

io.on('connection', (socket) => {
  var addedUser = false;
  var room;

  // when the client emits 'new message', this listens and executes
  socket.on('new message', (data) => {
    // we tell the client to execute 'new message'
    // socket.broadcast.emit('new message', {
    //   username: socket.username,
    //   message: data
    // });
    console.log('new message event')
    io.sockets.in(socket.room).emit('new message' , {
      username: socket.username,
      room: socket.room,
      message: data
    });
  });

  // when the client emits 'add user', this listens and executes
  socket.on('add user', (username, room) => {
    console.log('add user event')
    if (addedUser) return;
    
    // we store the username in the socket session for this client
    socket.username = username;
    socket.room = room;
    ++numUsers;
    addedUser = true;
    console.log('socket.username' , socket.username)
    console.log('socket.room' , socket.room)

    socket.join(room);
    
    io.sockets.in(socket.room).emit('login', {
      numUsers: numUsers
    });

    io.sockets.in(socket.room).emit('user joined' , {
      username: socket.username,
      room: socket.room,
      numUsers: numUsers
    })
  });

  // when the client emits 'typing', we broadcast it to others
  socket.on('typing', () => {
    console.log('typing event')
    io.sockets.in(socket.room).emit('typing' , {
      username: socket.username
    });
  });

  // when the client emits 'stop typing', we broadcast it to others
  socket.on('stop typing', () => {
    console.log('stop typing event')
    io.sockets.in(socket.room).emit('stop typing' , {
      username: socket.username
    });
  });

  // when the user disconnects.. perform this
  socket.on('disconnect', () => {
    console.log('disconnect event')

    if (addedUser) {
      --numUsers;
      
      io.sockets.in(socket.room).emit('user left' , {
        username: socket.username,
        numUsers : numUsers
      })
    }
  });
});
